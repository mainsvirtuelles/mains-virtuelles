﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;


public class the_game : MonoBehaviour {
	private int current_scene;
	private int current_turn;
	private string current_level;
	private string[] scenario;

	public GameObject mains_inertes;
	public GameObject mains_actives;
	public GameObject mains_actives_occulus;

	private string path;
	private string idName;

	// Use this for initialization
	void Start () {


		//récupération du nom de fichier
		path = PlayerPrefs.GetString ("fileName");
		idName = PlayerPrefs.GetString ("id");

		current_scene = PlayerPrefs.GetInt ("current_scene");
		current_turn = PlayerPrefs.GetInt ("current_turn");
		current_level = Application.loadedLevelName;

		//init log
		if (current_scene == 1 && current_turn == 1 && PlayerPrefs.GetInt ("started") == 0) {
			//writeLog("Identifiant expe : \t" + idName +"\n");
			PlayerPrefs.SetInt ("started", 1);
			//writeLog();
		}

		//on quitte si l'expe est finit // peut etre une scene de remerciements ... bref
		if (current_scene > 3) {
				current_scene = 0;
				//writeLog("\nFin de l'expérience.");
				//Application.LoadLevel ("menu");
				print ("fin de l'expérience!");
				Application.Quit();

			// /!\ SUPPRIMER ça avant le build!!!
			Application.LoadLevel("menu");

			}


		//mains_inertes = GameObject.Find ("mains_inertes");
		//mains_actives = GameObject.Find ("HandController");
		//mains_actives_occulus = GameObject.Find ("HandControllerOcculus");

		scenario = new string[]{
			"expe_balles",
			PlayerPrefs.GetString ("scenario1"),
			PlayerPrefs.GetString ("scenario2"),
			PlayerPrefs.GetString ("scenario3")
		};

		print (scenario [0] + " - " + scenario [1] + " - " + scenario [2] + " - " + scenario [3]);

		//PlayerPrefs.SetInt ("current_turn", current_turn+1);
		print ("scene :" + current_scene + " - tour : " + current_turn);

		switch (scenario[current_scene]) {
			case "expe_mains_actives"://1:
				
				//jeu des balles avant chaque occurence de "expe_mains_actives"
				if(PlayerPrefs.GetInt ("ballgame") == 0){
					Application.LoadLevel("expe_balles");
					break;
				}
				PlayerPrefs.SetInt ("ballgame",0);
				
				//fonctionnement normal.
				print ("mode : " + scenario[current_scene]);
				//init pour mains actives 
				mains_inertes.SetActive (false);
				mains_actives_occulus.SetActive (false);
				//writeLog ("\nscene : \t" + scenario[current_scene] + "\tturn : \t" + current_turn.ToString());
			 
				break;
			case "expe_mains_inertes"://2:
				print("mode : " + scenario[current_scene]);
				//init pour mains inertes
				mains_actives.SetActive (false);
				mains_actives_occulus.SetActive (false);
				//writeLog ("\nscene : \t" + scenario[current_scene] + "\tturn : \t" + current_turn.ToString());

				break;
			case "expe_pas_mains"://3:
				print("mode :" + scenario[current_scene]);
				//init pour pas de mains
				mains_inertes.SetActive (false);
				mains_actives.SetActive (false);
				mains_actives_occulus.SetActive (false);
				//writeLog ("\nscene : \t" + scenario[current_scene] + "\tturn : \t" + current_turn.ToString());

				break;
			default:
				print ("erreur!!!!!");
				//Application.Quit ();
				break;
		}





	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey("escape")) {
			Application.Quit ();
			Debug.Log ("bye bye!");
		}
		if (Input.GetKey ("x")) {
			if (current_turn < 4){
				next_turn();
			}
			else{
				//writeLog();
				next_scene();
			}
		}

	}

	void next_scene(){
		PlayerPrefs.SetInt ("current_scene", (current_scene+1));
		PlayerPrefs.SetInt ("current_turn", 1);
		Application.LoadLevel(current_level);
	}
	void next_turn(){
		PlayerPrefs.SetInt ("current_turn", (current_turn+1));
		Application.LoadLevel(current_level);
	}

	public void next(){
		if (current_turn < 4){
			next_turn();
		}
		else{
			//writeLog();
			next_scene();
		}
	}


	public void writeLog(string text="\n"){
		//System.IO.File.AppendAllText(path,text.ToString() + "\n" );
		using (StreamWriter sw = new StreamWriter(path, true)) {
			sw.WriteLine(text.ToString());	
		}
	}



}
