﻿using UnityEngine;

public class CustomHand : HandModel
{

    public Transform hand;
    public Transform foreArm;    

    /// <summary>
    /// Position initial du bras
    /// </summary>
    public Vector3 defaultArmPosition;

    /// <summary>
    /// Rotation initial du bras
    /// </summary>
    public Vector3 defaultArmRotation;
    

    /// <summary>
    /// Initialisation de la main
    /// </summary>
    public override void InitHand()
    {                 
        UpdateHand();
    }

    
    /// <summary>
    /// Appeler à chaque frame pour mettre à jour la position du bras, de l'avant bras et de la main    
    /// </summary>
    public override void UpdateHand()
    {

        // Mise à jour du bas
        if (foreArm != null)
        {
            foreArm.position = (GetArmCenter() + defaultArmPosition);
            foreArm.rotation = RotateArm(GetArmRotation()) * Quaternion.Euler(defaultArmRotation);
        }

        // Mise à jour de la paume
        if (hand != null)
        {
            hand.rotation = RotateHand(GetPalmRotation()) * Quaternion.Euler(defaultArmRotation);
        }
        
        // Mise à jour des doigts
        for (int i = 0; i < fingers.Length; ++i)
        {

            if (fingers[i] != null)
            {
                //fingers[i].UpdateFinger();
            }
        }        
    }

    /// <summary>
    /// Rotation du bras
    /// </summary>
    /// <param name="qIn">Angle de rotation non transformé</param>
    /// <returns>Angle de rotation transformé</returns>
    public Quaternion RotateArm(Quaternion qIn)
    {
        // in x result z
        // in y result -y
        // in z result x

        var x = qIn.x;
        qIn.x = qIn.z;
        qIn.z = qIn.y;
        //qIn.z = x;

        return qIn;
    }

    /// <summary>
    /// Rotation de la main
    /// </summary>
    /// <param name="qIn">Angle de rotation non transformé</param>
    /// <returns>Angle de rotation transformé</returns>
    public Quaternion RotateHand(Quaternion qIn)
    {
        var x = qIn.x;
        qIn.x = qIn.z;// - foreArm.rotation.x;
        qIn.y = qIn.y;
        qIn.z = -x;
        return qIn;
    }
}
