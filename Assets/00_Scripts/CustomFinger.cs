﻿using Leap;
using UnityEngine;

public class CustomFinger : FingerModel
{
    // Composition des doigts
    public Transform[] bones = new Transform[NUM_BONES];

    /// <summary>
    /// Initialisation des doigts
    /// </summary>
    public override void InitFinger()
    {
        UpdateFinger();
    }

    /// <summary>
    /// Mise à jour des doigts
    /// </summary>
    public override void UpdateFinger()
    {
        for (int i = 0; i < bones.Length; ++i)
        {
            if (bones[i] != null)
            {
                //Transformation pour correspondance des doigts
                var q = GetBoneRotation(i);
                var z = q.z;
                q.z = q.x;
                q.x = q.y;
                q.y = z;
                bones[i].rotation = q;
            }
                
        }
    }
}
