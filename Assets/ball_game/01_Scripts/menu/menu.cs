﻿using UnityEngine;
using System.Collections;

public class menu : MonoBehaviour {

	public Object scene;
	public Texture default_texture;
	public Texture hover_texture;

	// Use this for initialization
	void Start () {
		renderer.material.mainTexture = default_texture;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKey("escape")) {
			Application.Quit ();
			//Debug.Log (this.name);
		}
	}

	void OnMouseEnter(){
		renderer.material.mainTexture = hover_texture;
	}
	void OnMouseExit(){
		renderer.material.mainTexture = default_texture;
	}
	//au clic sur un des menus, on charge la scene spécifiée.
	void OnMouseDown(){
		//Debug.Log (this.name);
		if (!scene) {
			Application.Quit ();
			//Debug.Log (this.name);
		} else {
			Application.LoadLevel (scene.name);
		}

	}
}
