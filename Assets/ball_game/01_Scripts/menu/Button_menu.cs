﻿using UnityEngine;
using System.Collections;

public class Button_menu : MonoBehaviour {

	private string scene;

	public Texture default_texture;
	public Texture hover_texture;

	protected string choixScene = "";

	//définition des scenes existantes
	private string[] scenarpart = new string[]{"balle_game", "expe_mains_actives", "expe_mains_inertes", "expe_pas_mains"};



	public GUIText info;
	public string information;
	
	// Use this for initialization
	void Start () {

		scene = "expe_mains_actives";

		info.text = "Passez la souris sur un élément du menu.";

		renderer.material.mainTexture = default_texture;

		//initialisation des scenes et tours
		PlayerPrefs.SetInt("current_scene",1);
		PlayerPrefs.SetInt ("current_turn", 1);
	}





	// Update is called once per frame
	void Update () {

	}






	void OnMouseEnter(){
		renderer.material.mainTexture = hover_texture;
		info.text = this.information;
		//Debug.Log (this.name);
	}
	void OnMouseExit(){
		info.text = "Passez la souris sur un élément du menu.";
		renderer.material.mainTexture = default_texture;
	}

	//au clic sur un des menus, enregistrement du scenario puis chargement scene spécifiée.
	void OnMouseDown(){

		if (this.name == "Quitter") {
			PlayerPrefs.DeleteAll ();
			Application.Quit ();
			Debug.Log ("au revoir!");
		}
		if (this.name == "Jeu_balles") {
			Application.LoadLevel("expe_balles");
		}



		//mains actives first
		if (this.name == "1") {
			PlayerPrefs.SetString("ordre", this.name );
			//print(this.name);
			PlayerPrefs.SetString("scenario1", scenarpart[1] );
			PlayerPrefs.SetString("scenario2", scenarpart[2] );
			PlayerPrefs.SetString("scenario3", scenarpart[3] );
			print ("le scénario sera : " + PlayerPrefs.GetString("scenario1") + " " + PlayerPrefs.GetString("scenario2") + " " + PlayerPrefs.GetString("scenario3"));
			Application.LoadLevel(scene);
		}
		if (this.name == "2") {
			PlayerPrefs.SetString("ordre", this.name );
			PlayerPrefs.SetString("scenario1", scenarpart[1] );
			PlayerPrefs.SetString("scenario2", scenarpart[3] );
			PlayerPrefs.SetString("scenario3", scenarpart[2] );
			print ("le scénario sera : " + PlayerPrefs.GetString("scenario1") + " " + PlayerPrefs.GetString("scenario2") + " " + PlayerPrefs.GetString("scenario3"));
			Application.LoadLevel(scene);
		}

		//mains inertes first
		if (this.name == "3") {
			PlayerPrefs.SetString("ordre", this.name );
			PlayerPrefs.SetString("scenario1", scenarpart[2] );
			PlayerPrefs.SetString("scenario2", scenarpart[3] );
			PlayerPrefs.SetString("scenario3", scenarpart[1] );
			print ("le scénario sera : " + PlayerPrefs.GetString("scenario1") + " " + PlayerPrefs.GetString("scenario2") + " " + PlayerPrefs.GetString("scenario3"));
			Application.LoadLevel(scene);
		}
		if (this.name == "4") {
			PlayerPrefs.SetString("ordre", this.name );
			PlayerPrefs.SetString("scenario1", scenarpart[2] );
			PlayerPrefs.SetString("scenario2", scenarpart[1] );
			PlayerPrefs.SetString("scenario3", scenarpart[3] );
			print ("le scénario sera : " + PlayerPrefs.GetString("scenario1") + " " + PlayerPrefs.GetString("scenario2") + " " + PlayerPrefs.GetString("scenario3"));
			Application.LoadLevel(scene);
		}

		//pas de mains first
		if (this.name == "5") {
			PlayerPrefs.SetString("ordre", this.name );
			PlayerPrefs.SetString("scenario1", scenarpart[3] );
			PlayerPrefs.SetString("scenario2", scenarpart[1] );
			PlayerPrefs.SetString("scenario3", scenarpart[2] );
			print ("le scénario sera : " + PlayerPrefs.GetString("scenario1") + " " + PlayerPrefs.GetString("scenario2") + " " + PlayerPrefs.GetString("scenario3"));
			Application.LoadLevel(scene);
		}
		if (this.name == "6") {
			PlayerPrefs.SetString("ordre", this.name );
			PlayerPrefs.SetString("scenario1", scenarpart[3] );
			PlayerPrefs.SetString("scenario2", scenarpart[2] );
			PlayerPrefs.SetString("scenario3", scenarpart[1] );
			print ("le scénario sera : " + PlayerPrefs.GetString("scenario1") + " " + PlayerPrefs.GetString("scenario2") + " " + PlayerPrefs.GetString("scenario3"));
			Application.LoadLevel(scene);
		}


	}
}

