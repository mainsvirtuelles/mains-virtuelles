﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Jeu_changements_textures : MonoBehaviour {
	
	public Texture default_texture;
	//public Texture end_texture;
	public Texture feuillePlusDroite;
	public Texture feuillePlusGauche;
	public Texture feuilleCroixDroite;
	public Texture feuilleCroixGauche;
	public Texture feuilleNoireCarresVides;
	//public Texture feuilleNoireCarresVides2;
	public Texture PasRepondre;
	public Texture DoitRepondre;

	public the_game thegamescript;
	
	

	float temps = 0.0f;
	float lasts = 1.5f;
	float fixationTime=1.0f;
	
	
	float time =0.0f;
	float deltaTime=1.0f;
	
	int loadtexture;

	int a=8;
	int b=8;
	int c=8;
	int d=8;

	bool g= true;
	int current_test=0;

	private int nombreStimuli;  // permet de choisir le nombre de stimuli total 
	private string idName;
	private string ordre;

	private int current_scene;
	private int current_turn;
	
	
	private string consigne;
	private string consigne2;
	
	void Start ()
	{
		nombreStimuli = PlayerPrefs.GetInt("nbStimuli");
		a=nombreStimuli/4;
		b=nombreStimuli/4;
		c=nombreStimuli/4;
		d=nombreStimuli/4;
		consigne = "feuillePlusDroite3";
		consigne2 = "feuillePlusGauche3";

		idName = PlayerPrefs.GetString ("id");
		ordre = PlayerPrefs.GetString("ordre");

		current_scene = PlayerPrefs.GetInt ("current_scene");
		current_turn = PlayerPrefs.GetInt ("current_turn");



		//thegamescript.writeLog ("les consignes sont : \t"+ consigne + "\t" + consigne2);
		
		
		
		
		//texture par défaut
		//default_texture = renderer.material.mainTexture;
		//renderer.material.mainTexture = myTextures[arrayPos];
		renderer.material.mainTexture = default_texture;
		
	}
	
	void Changement_Texture()
	{
		int numero_texture = Random.Range(1, 5);
		if (numero_texture==1 && a!=0 )
		{
			current_test++;
			temps=fixationTime;
			renderer.material.mainTexture = feuillePlusDroite;
			Debug.Log("Texture : " + "plusDroite");
			a--;
			g=false;
			
		}
		
		else if (numero_texture==2 && b!=0  )
		{
			current_test++;
			temps=fixationTime;
			renderer.material.mainTexture = feuillePlusGauche;
			Debug.Log("Texture : " + "plusGauche");
			b--;
			g=false;
			
		}
		
		else if (numero_texture==3 && c!=0  )
		{
			current_test++;
			temps=fixationTime;
			renderer.material.mainTexture = feuilleCroixGauche;
			Debug.Log("Texture : " + "CroixGauche");
			c--;
			g=false;
			
		}
		else if (numero_texture==4 && d!=0 )
		{
			current_test++;
			temps=fixationTime;
			renderer.material.mainTexture = feuilleCroixDroite;
			Debug.Log("Texture : " + "CroixDroite");
			d--;
			g=false;
		}
		
		
		else {
			if (a==0 && b==0 && c==0 && d==0)
			{
				//Application.LoadLevel(Application.loadedLevelName);
				thegamescript.next();

			}
			else{
				Changement_Texture();
			}
			
		}
		
		
	}
	void Update ()
	{	
		
		
		
		temps += Time.deltaTime;
		
		if (temps < fixationTime && temps>0) {
			
			renderer.material.mainTexture = feuilleNoireCarresVides;

			
		}
		if (Input.GetKeyDown ("p")) {//la cible est affichée on répond

			// bonne réponse
			if((renderer.material.mainTexture.name == consigne || renderer.material.mainTexture.name == consigne2 || renderer.material.mainTexture.name == PasRepondre.name || renderer.material.mainTexture.name == DoitRepondre.name) && temps >= 0.0f){
				//print (temps-fixationTime);
				if(renderer.material.mainTexture.name == consigne){
					//répondu ok droit
					thegamescript.writeLog (idName + "\t" + ordre + "\t" + current_scene + "\t" + current_turn + "\t"+current_test + "\t2\t2\t1\t1\t0\t" + (temps-fixationTime));
				}
				else if(renderer.material.mainTexture.name == consigne2){
					//répondu ok gauche
					thegamescript.writeLog (idName + "\t" + ordre + "\t" + current_scene + "\t" + current_turn +"\t"+ current_test + "\t1\t2\t1\t1\t0\t" + (temps-fixationTime));
				}																											
				print ("bien répondu");
				temps=0.0f;
				g=true;
				
			}
			//mauvaise réponse
			else if ((renderer.material.mainTexture.name != consigne && renderer.material.mainTexture.name != consigne2 && renderer.material.mainTexture.name != PasRepondre.name && renderer.material.mainTexture.name != DoitRepondre.name)&& temps >= 0.0f)
			{
				//répondu mauvais droit
				if(renderer.material.mainTexture == feuilleCroixDroite){
					thegamescript.writeLog (idName + "\t" + ordre + "\t" + current_scene + "\t" + current_turn +"\t"+ current_test + "\t2\t1\t1\t2\t1\t" + (temps-fixationTime));
				}
				//répondu mauvais gauche
				else if(renderer.material.mainTexture == feuilleCroixGauche){
					thegamescript.writeLog (idName + "\t" + ordre + "\t" + current_scene + "\t" + current_turn +"\t"+ current_test + "\t1\t1\t1\t2\t1\t" + (temps-fixationTime));
				}
				temps=-1.0f;
				renderer.material.mainTexture = PasRepondre;
				print ("mauvaise réponse");
				g=true;
			}
			
		}
		//la cible n'est pas affichée on ne répond pas.(bonne réponse)
		else if ( temps >= fixationTime && g)
		{	

			Changement_Texture();

		}  
		else if (temps > fixationTime + 1.5 ) {	//la cible est affichée on ne répond pas																				//positionStim/Typestim/RepPart/TypeRep/CodeErreu/
			g=true;
			//pas répondu ok droit
			if(renderer.material.mainTexture.name == "feuilleCroixDroite3"){
				thegamescript.writeLog (idName + "\t" + ordre + "\t" + current_scene + "\t" + current_turn +"\t"+ current_test + "\t2\t1\t2\t4\t0\t" + "1.5"); 
				temps=0.0f;
			}
			//pas répondu ok gauche
			else if(renderer.material.mainTexture.name == "feuilleCroixGauche3"){
				thegamescript.writeLog (idName + "\t" + ordre + "\t" + current_scene + "\t" + current_turn +"\t"+ current_test + "\t1\t1\t2\t4\t0\t" + "1.5");
				temps=0.0f;
			}
			//pas répondu aurait dut droit 
			else if(renderer.material.mainTexture.name == consigne){
				thegamescript.writeLog (idName + "\t" + ordre + "\t" + current_scene + "\t" + current_turn +"\t"+ current_test + "\t2\t2\t2\t3\t1\t" + "1.5");
				renderer.material.mainTexture = DoitRepondre;
				temps=-1.0f;
			}
			//pas répondu aurait dut gauche
			else if(renderer.material.mainTexture.name == consigne2){
				thegamescript.writeLog (idName + "\t" + ordre + "\t" + current_scene + "\t" + current_turn +"\t"+ current_test + "\t1\t2\t2\t3\t1\t" + "1.5");
				renderer.material.mainTexture = DoitRepondre;
				temps=-1.0f;
			}
			else{
				//sinon
				temps=0.0f;
			}
		}
	}
}





