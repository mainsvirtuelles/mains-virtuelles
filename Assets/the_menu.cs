﻿using UnityEngine;
using System.Collections;
using System.IO;

public class the_menu : MonoBehaviour {

	public GUIText id;
	private string fileName;


	// Use this for initialization
	void Start () {

//	================	initialisation du jeu   ======================================= 


		//on efface toutes les préférences des autres parties 
		PlayerPrefs.DeleteAll ();

		//création du dossier sim'il n'existe particleSystem déjà.
		System.IO.Directory.CreateDirectory ("./logs");

		// /!\ utilisés par le jeu ( == variables "globales" ) /!\
		PlayerPrefs.SetInt ("ballgame", 0);
		PlayerPrefs.SetInt ("started", 0);





		//Réglages ....

		//le nom du joueur
		string idName = System.DateTime.Now.ToString ("yyyyMMddTHHmmssff");
		PlayerPrefs.SetString ("id", idName);

		//le nom du fichier de log
		fileName = "./logs/" + idName + ".csv";
		id.text = "ID : " + idName;
		PlayerPrefs.SetString ("fileName", fileName);

		//nb stimuli par bloc
		PlayerPrefs.SetInt("nbStimuli", 32);

		//nb buts verts touchés pour revenir à l'expérience
		PlayerPrefs.SetInt("Goalstowin", 20);




	}




	// Update is called once per frame
	void Update () {
		if (Input.GetKey("escape")) {
			Application.Quit ();
			Debug.Log ("bye bye!");
		}
	}
}
